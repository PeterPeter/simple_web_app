# -*- coding: utf-8 -*-

# Copyright (C) 2017 Peter Pfleiderer
#
# This file is part of regioclim.
#
# regioclim is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# regioclim is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.    See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License
# along with regioclim; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA    02110-1301    USA

import os,glob,sys,time,random,pickle,string,gc,subprocess,importlib
from app import app
from flask import redirect, render_template, url_for, request, flash, get_flashed_messages, g, session, jsonify, Flask, send_from_directory
import matplotlib.pylab as plt
from app.plotting import *

from app import settings
from app import forms
from app import plotting

import class_on_shape; importlib.reload(class_on_shape)

# this is the initial landing page
# here default choices are defined
@app.route('/')
def index():
	session['country']     = 'BEN'
	return redirect(url_for("choices"))

# this is the main page
@app.route('/choices')
def choices():
	print(session)

	# forms are dropdown menues in this example
	# they are defined in forms.py
	form_country = forms.countryForm(request.form)
	# here I just resort the country names so that the selected country is the first choice
	countries_available = [session['country']]+[cou for cou in sorted(settings.country_names.keys()) if cou != session['country']]
	# here the choices for the form are defined
	form_country.countrys.choices = zip(countries_available,[settings.country_names[cou] for cou in countries_available])


	# in this dict all variables (forms, text, etc.) that is required by choices.html has to be defined
	context = {
		'some_text': 'bla bla bla',
		'plot1': 'static/plots/'+session['country']+'/'+session['country']+'_plot1.png',
		'plot2': 'static/plots/'+session['country']+'/'+session['country']+'_plot2.png',
		'plot3': 'static/plots/'+session['country']+'/'+session['country']+'_plot3.png',
		'form_country':form_country,
	}

	return render_template('choices.html',**context)


###############################
# option choices
###############################

# this route is called, when a new country is selected
@app.route('/country_choice',    methods=('POST', ))
def country_choice():
	# select the right form
	form_country = forms.countryForm(request.form)
	# get the current value of that form
	session['country'] = form_country.countrys.data
	return redirect(url_for('choices'))


###############################
# Navigation
###############################

@app.route('/go_to_choices',    methods=("POST", ))
def go_to_choices():
	return redirect(url_for("choices"))

@app.route('/home',    methods=('GET', ))
def render_home():
	return redirect(url_for('index'))

@app.route('/about',    methods=('GET', ))
def render_about():
	return render_template('about.html')

@app.route('/contact',    methods=('GET', ))
def render_contact():
	return render_template('contact.html')

@app.route('/documentation')
def documentation():
	return render_template('documentation.html')

@app.route('/error')
def error():
	return render_template('error.html')






#
