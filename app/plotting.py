# -*- coding: utf-8 -*-

# Copyright (C) 2017 Peter Pfleiderer
#
# This file is part of regioclim.
#
# regioclim is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# regioclim is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License
# along with regioclim; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


import os,sys,importlib,time

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy
import numpy as np

from app import settings

sys.path.append('../climate_data_on_shapes')
import class_on_shape; importlib.reload(class_on_shape)



def plot_something(session,COU):
	'''
	here you can plot what ever you want
	'''
	COU.load_areaAverage()

	# select some data
	tmp = COU.select_areaAverage('monthly',tags={'source':'JRA55','var_name':session['indicator'],'region':session['region'],'mask_style':'latWeight'})

	# find relevant months for the selected season
	relevant_months = np.zeros(tmp.time.shape[0], np.bool)
	for mon in settings.season_dict[session['season']]['months']:
		relevant_months[tmp.time.dt.month.values == mon] = True

	# compute seasonal mean
	tmp = tmp[relevant_months]
	tmp = tmp.groupby('time.year').mean('time')

	plt.close('all')
	fig,ax = plt.subplots(nrows=1, figsize=(5,4))
	ax.plot(tmp.year.values,tmp.values, color='green')
	ax.set_ylabel(settings.indicator_dict[session['indicator']]['name']+settings.indicator_dict[session['indicator']]['unit'])
	ax.set_xlabel('year')
	ax.set_title(u''+COU._region_names[session['region']].replace('_',' ')+' '+settings.season_dict[session['season']]['name'].replace('*',''),fontsize=12)

	# save it in static/..
	plot_file_name='static/COU_images/'+session['country']+'/'+session['indicator']+'_'+session['region']+'_'+session['season']+'_transient'+'.png'
	plt.savefig('app/'+plot_file_name, bbox_inches='tight')

	return plot_file_name

def plot_something_else(session,COU):
	'''
	here you can plot what ever you want
	'''

	COU.read_griddedData()

	# select some data
	tmp = COU.select_griddedData('monthly',tags={'source':'JRA55','var_name':session['indicator']})

	# find relevant months for the selected season
	relevant_months = np.zeros(tmp.time.shape[0], np.bool)
	for mon in settings.season_dict[session['season']]['months']:
		relevant_months[tmp.time.dt.month.values == mon] = True

	# compute seasonal mean
	tmp = tmp[relevant_months]
	tmp_seasonal = tmp.groupby('time.year').mean('time')
	tmp_period = tmp_seasonal.mean('year')

	plt.close('all')
	x,y=tmp_period.lon.copy(),tmp_period.lat.copy()
	fig,ax = plt.subplots(nrows=1, figsize=(4,4*(len(y)/len(x))**0.5),subplot_kw={'projection': ccrs.PlateCarree()})
	ax.coastlines(resolution='10m');
	x-=np.diff(x,1)[0]/2.
	y-=np.diff(y,1)[0]/2.
	x=np.append(x,[x[-1]+np.diff(x,1)[0]])
	y=np.append(y,[y[-1]+np.diff(y,1)[0]])
	x,y=np.meshgrid(x,y)

	im = ax.pcolormesh(x,y,tmp_period, cmap='plasma', transform=ccrs.PlateCarree())
	for shape in COU._region_polygons.values():
		ax.add_geometries([shape], ccrs.PlateCarree(), edgecolor='k',alpha=1,facecolor='none',linewidth=0.5,zorder=50)
	cb = plt.colorbar(im, ax=ax)
	cb.set_label(settings.indicator_dict[session['indicator']]['name']+settings.indicator_dict[session['indicator']]['unit'], rotation=90)
	ax.set_title(settings.season_dict[session['season']]['name'].replace('*',''),fontsize=12)

	# save it in static/..
	plot_file_name='static/COU_images/'+session['country']+'/'+session['indicator']+'_'+session['region']+'_'+session['season']+'_map'+'.png'
	plt.savefig('app/'+plot_file_name, bbox_inches='tight')

	return plot_file_name










#
